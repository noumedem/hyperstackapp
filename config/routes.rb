Rails.application.routes.draw do
  root 'hyperstack#HelloWorld'
  mount Hyperstack::Engine => '/hyperstack'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
