class HyperComponent
  include Hyperstack::Component
  include Hyperstack::Router::Helpers # if you are using the router
  include Hyperstack::State::Observable
end
