class Counter < HyperComponent
  before_mount do
    @count = 0 # optional initialization
  end

  render(DIV) do
    BUTTON { "+" }.on(:click) { mutate @count += 1 }
      P { @count.to_s } # note how we access the count variable
  end
end
