class Indenter < HyperComponent
  render(DIV) do
    IndentEachLine(by: 100) do # see IndentEachLine below
      DIV {"Line 1"}
      DIV {"Line 2"}
      DIV {"Line 3"}
    end
  end
end
