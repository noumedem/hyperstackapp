class HelloWorld < HyperComponent
  param visitor: "World", type: String

  render(DIV)do

      H1 { "Hello #{@Visitor}"}
      H1 { "Hello world from Hyperstack edge!" }

      UL do
        10.times { |n| LI { "Number #{n}" }}
      end

      DIV(class: 'green-text') { "Let's gets started!" }

      TABLE(class: 'ui celled table') do
        THEAD do
          TR do
            TH(style: {color: 'red'}) { 'One' }
            TH { 'Two' }
            TH { 'Three' }
          end
        end
        TBODY do
          TR do
            TD { 'A' }
            TD(class: 'negative') { 'B' }
            TD { 'C' }
          end
        end
      end

      A(href: '/') { 'Click me' }

      P(class: :blue, style: {color: 'blue'}) {"Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker."}
      NextComponent()
      Indenter()
      Counter()
      LikeButton()

      UsingState()
  end
end
