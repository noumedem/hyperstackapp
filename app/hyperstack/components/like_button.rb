class LikeButton < HyperComponent
  render(DIV) do
    BUTTON do
      "You #{@liked ? 'like' : 'haven\'t liked'} this. Click to toggle."
    end.on(:click) do
      mutate @liked = !@liked
    end
  end
end
