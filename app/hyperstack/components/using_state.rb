class UsingState < HyperComponent

  render(DIV) do
    # the button method returns an HTML element
    # .on(:click) is an event handeler
    # notice how we use the mutate method to get
    # React's attention. This will cause a
    # re-render of the Component
      button.on(:click) { mutate(@show = !@show) }
    DIV do
      input
      output
      easter_egg
    end if @show
  end

  def button
    BUTTON(class: 'ui primary button') do
      @show ? 'Hide' : 'Show'
    end
  end

  def input
    DIV(class: 'ui input fluid block') do
      INPUT(type: :text).on(:change) do |evt|
        # we are updating the value per keypress
        # using mutate will cause a rerender
        mutate @input_value = evt.target.value
      end
    end
  end

  def output
    # rerender whenever input_value changes
    P { "#{@input_value}" }
  end

  def easter_egg
    H2 {'you found it!'} if @input_value == 'egg'
  end
end
